package id.solusikirim.api;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;

public class EskaAPI
{
    public static String baseURl = "https://backend-dev.solusikirim.id";


    public String getToken()
    {
        String token = null;
        JSONObject reqBody = new JSONObject();
        reqBody.put("email", "eska.adi.diskon.kurir@gmail.com");
        reqBody.put("password", "Eska12345");

        RestAssured.baseURI =  baseURl;

        token =
        given().
                body(reqBody.toJSONString()).
                contentType("application/json").
        when().
                post(baseURl+"/delivery-platform-backend/login").
        then().
                extract()
                .path("data.access_token");

        return token;
    }

    @Test(testName = "Get Dashboard COD Delivered")
    public void getDasboardCodDelivered()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","true")
                .queryParam("interval","1")
                .queryParam("status","17")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
        then()
                .log().all().

         assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());

    }

    @Test(testName = "Get Dashboard COD On Process")
    public void getDasboardCodOnProcess()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","true")
                .queryParam("interval","1")
                .queryParam("status","103")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());

    }

    @Test
    public void getDasboardCodUnDelivered()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","true")
                .queryParam("interval","1")
                .queryParam("status","104")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());
    }

    @Test
    public void getDasboardCodReturn()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","true")
                .queryParam("interval","1")
                .queryParam("status","105")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());

    }

    @Test
    public void getDasboardCodReturned()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","true")
                .queryParam("interval","1")
                .queryParam("status","106")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());

    }

    @Test
    public void getDasboardNonCodDelivered()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","false")
                .queryParam("interval","1")
                .queryParam("status","17")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());

    }

    @Test
    public void getDasboardNonCodOnProcess()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","false")
                .queryParam("interval","1")
                .queryParam("status","103")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());

    }

    @Test
    public void getDasboardNonCodUnDelivered()
    {
        RestAssured.baseURI =  baseURl;
        given()
                .queryParam("courier","JNE")
                .queryParam("is_cod","false")
                .queryParam("interval","1")
                .queryParam("status","104")
                .header("Authorization","Bearer "+getToken())
                .get("/delivery-platform-backend/total-order-detail").
                then()
                .log().all().
                assertThat()
                .statusCode(200)
                .body("message.English", Matchers.equalTo("Success"))
                .body("data.total",notNullValue());
    }

}
